# Eaton Ellipse Pro 650 outages notifier
A simple Python script that can sends you emails when a power outages occurs and is detected by the Eaton Ellipse Pro 650 UPS. This requires that your internet connection is powered by the UPS itself or else the email cannot be sent.

## Requirements
* Python >=3.10
* An internet connection
* An email account (a Gmail or Outlook personal account is enough)

## Installation
1. Clone this repository or download as a ZIP
2. Create a copy of the `.env.example` file and name it `.env`
3. Edit the `.env` file so that it has a correct setup (more below)
4. Start the Eaton UPS Companion app
5. Launch it via a terminal to see if it works: `python main.py`
6. If it works, set it up to work as a Scheduled Job (Windows) or a cronjob (Unix/Linux) to automatically run this script each N minutes/hours

## .env configuration
`EATON_COMPANION_HOST="http://localhost:4679/server"` this is the address where the Eaton UPS Companion app launches its web server

`EMAIL_SENDER_ADDRESS=""` this is the e-mail address that will be sending the emails

`EMAIL_SENDER_PASSWORD=""` this is the password of the e-mail address that will be sending the emails

`EMAIL_SMTP=""` this is the address of the SMTP server that will be sending the emails

`EMAIL_PORT=` this is the port of the SMTP server that will be sending the emails

`EMAIL_TO=""` the receiver of the emails. It supports multiple values separated by commas (eg: hello@apple.com,welcome@google.com)

## Email configuration
### Google
| Key | Values |
| ----------------------------------------- | ------------------------------------------------------------ |
| EMAIL_SMTP     | smtp.gmail.com |
| EMAIL_PORT | 587            |

[Official documentation](https://support.google.com/mail/answer/7126229?hl=en#zippy=%2Cstep-change-smtp-other-settings-in-your-email-client)

### Outlook
| Key | Values |
| ----------------------------------------- | ------------------------------------------------------------ |
| EMAIL_SMTP     | smtp-mail.outlook.com |
| EMAIL_PORT | 587            |

[Official documentation](https://support.microsoft.com/en-us/office/pop-imap-and-smtp-settings-for-outlook-com-d088b986-291d-42b8-9564-9c414e2aa040)
