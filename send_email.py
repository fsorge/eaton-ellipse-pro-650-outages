import smtplib
import ssl
from typing import List

class Email:
    def __init__(self, to: List[str], subject: str, body: str) -> None:
        self.to = to
        self.subject = subject
        self.body = body


class SendEmail:
    def __init__(self, sender_address: str, sender_password: str, smtp: str, port: int) -> None:
        self.__sender_address = sender_address
        self.__sender_password = sender_password
        self.__smtp = smtp
        self.__port = port

    def send(self, email: Email) -> int:
        # Create a secure SSL context
        context = ssl.create_default_context()
        success = 0

        with smtplib.SMTP(self.__smtp, self.__port) as server:
            server.starttls(context=context)
            server.login(self.__sender_address, self.__sender_password)

            for to in email.to:
                success = success + 1
                server.sendmail(self.__sender_address, to, f"Subject: {email.subject}\n\n{email.body}")
        
        return success