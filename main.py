import requests
import os
import urllib.parse
from dotenv import load_dotenv

from send_email import Email, SendEmail

def normalize_data(s: str):
    return s.replace('&nbsp;', ' ')

load_dotenv()

dataResponse = requests.get(
    urllib.parse.urljoin(
        os.getenv('EATON_COMPANION_HOST'),
        "data_srv.js?action=getData&from=webUI"
    )
)

responseJson = dataResponse.json()

if dataResponse.status_code == 200:
    lastDate = responseJson['lastDate']
    data = responseJson['data']

    currentState = data['activeState']

    # If a power failue is ongoing 
    if currentState != 'acpresent':
        power = normalize_data(data['power'])
        energy = normalize_data(data['energy'])
        battery = {
            'level': normalize_data(data['batteryCap']),
            'remainingEstimate': normalize_data(data['batteryRunTime'])
        }

        tos = os.getenv('EMAIL_TO').split(',')

        email = Email(
            tos,
            "Interruzione di energia elettrica",
            f"""\
Batteria: {battery['level']} con un\'autonomia di {battery['remainingEstimate']}
Potenza erogata: {power}
Energia: {energy}\
            """
        )

        print(f"{email.body}\n")

        sendEmail = SendEmail(
            os.getenv('EMAIL_SENDER_ADDRESS'),
            os.getenv('EMAIL_SENDER_PASSWORD'),
            os.getenv('EMAIL_SMTP'),
            os.getenv('EMAIL_PORT')
        )
        res = sendEmail.send(email)

        if res == len(tos):
            print("Email(s) sent successfully")
        else:
            print(f"Something wrong happened. Sent {res} email(s) of {len(tos)}")
else:
    print("Power is on")


